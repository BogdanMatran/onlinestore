//
//  UIViewExtension.swift
//  OnlineStore
//
//  Created by Matran, Bogdan on 26/06/2018.
//  Copyright © 2018 Matran, Bogdan. All rights reserved.
//

import UIKit

extension UIView {
    func addShadow(with radius: CGFloat, and opacity: Float) {
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = radius
        self.layer.shadowColor = UIColor.brown.cgColor
        self.layer.shadowOpacity = opacity
    }
    
}

extension UIView {
    func set(borderColor: CGColor? = nil, borderWidth: CGFloat? = nil, cornerRadius: CGFloat) {
        if borderWidth != nil {
            self.layer.borderWidth = borderWidth!
        }
        if borderColor != nil {
            self.layer.borderColor = borderColor!
        }
        self.layer.cornerRadius = cornerRadius
    }
}
