//
//  UserDefaultsExtension.swift
//  OnlineStore
//
//  Created by Matran, Bogdan on 26/06/2018.
//  Copyright © 2018 Matran, Bogdan. All rights reserved.
//

import Foundation
import UIKit

extension UserDefaults {
    func containsConnectData() -> Bool {
        let defaults = UserDefaults.standard
        if defaults.string(forKey: "name") != nil && defaults.string(forKey: "phone") != nil && defaults.string(forKey: "address") != nil {
            return true
        }
        return false
    }
}
