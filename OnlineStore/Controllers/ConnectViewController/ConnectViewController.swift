//
//  ConnectViewController.swift
//  OnlineStore
//
//  Created by Matran, Bogdan on 14/06/2018.
//  Copyright © 2018 Matran, Bogdan. All rights reserved.
//

import Foundation
import UIKit


class ConnectViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate {
    
    @IBOutlet weak var connectSaveButton: UIButton!
    @IBOutlet weak var disconnectButton: UIButton!
    @IBOutlet weak var connectButton: UIButton!
    @IBOutlet weak var addressTextView: UITextView!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    fileprivate let borderWidthValue: CGFloat = 0.5
    fileprivate let borderColorValue: CGColor = UIColor.lightGray.cgColor
    fileprivate let cornerRadiusValue: CGFloat = 8
    fileprivate let radiusValue: CGFloat = 6
    fileprivate let opacityValue: Float = 0.75
    
    @IBAction func didTapOnConnectSaveButton(_ sender: Any) {
        let dataValidity = validateConnectData()
        if dataValidity {
            saveToUserDefaults()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func didTapOnConnectButton(_ sender: Any) {
        let dataValidity = validateConnectData()
        if dataValidity {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func didTapOnDisconnectButton(_ sender: Any) {
        let defaults = UserDefaults.standard
        defaults.set(nil, forKey: "name")
        nameTextField.text = ""
        defaults.set(nil, forKey: "phone")
        phoneTextField.text = ""
        defaults.set(nil, forKey: "address")
        addressTextView.text = ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        populateWithUserDefaultsValues()
        decorateConnectScreen()
        nameTextField.delegate = self
        phoneTextField.delegate = self
        addressTextView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        connectButton.addShadow(with: radiusValue, and: opacityValue)
        connectSaveButton.addShadow(with: radiusValue, and: opacityValue)
        disconnectButton.addShadow(with: radiusValue, and: opacityValue)
    }
    
    func decorateConnectScreen() {
        addImage(to: nameTextField, name: "user-connect")
        addImage(to: phoneTextField, name: "customer-service")
        nameTextField.set(borderColor: borderColorValue, borderWidth: borderWidthValue, cornerRadius: cornerRadiusValue)
        phoneTextField.set(borderColor: borderColorValue, borderWidth: borderWidthValue, cornerRadius: cornerRadiusValue)
        addressTextView.set(borderColor: borderColorValue, borderWidth: borderWidthValue, cornerRadius: cornerRadiusValue)
        disconnectButton.set(cornerRadius: cornerRadiusValue)
        connectButton.set(cornerRadius: cornerRadiusValue)
        connectSaveButton.set(cornerRadius: cornerRadiusValue)
    }
    
    func saveToUserDefaults() {
        let defaults = UserDefaults.standard
        defaults.set(nameTextField.text, forKey: "name")
        defaults.set(phoneTextField.text, forKey: "phone")
        defaults.set(addressTextView.text, forKey: "address")
    }
    
    func validateConnectData() -> Bool {
        if nameTextField.text != "" && phoneTextField.text != "" && addressTextView.text != "" {
            print("Saved")
            return true
        } else {
            print("Alert mode")
            let alert = UIAlertController(title: "Error", message: "All fields are mandatory in order to connect!", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
            self.present(alert, animated: true, completion: nil)
            return false
        }
    }
    
    func populateWithUserDefaultsValues() {
        let defaults = UserDefaults.standard
        if defaults.containsConnectData() {
            nameTextField.text = defaults.string(forKey: "name")
            phoneTextField.text = defaults.string(forKey: "phone")
            addressTextView.text = defaults.string(forKey: "address")
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case nameTextField:
            phoneTextField.becomeFirstResponder()
        case phoneTextField:
            phoneTextField.resignFirstResponder()
        default:
            textField.resignFirstResponder()
        }
        return false
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        if(text == "\n") {
            view.endEditing(true)
            return false
        } else {
            return true
        }
    }
    
    func addImage(to textField: UITextField, name: String) {
        let imageView = UIImageView();
        let image = UIImage(named: name);
        imageView.image = image;
        imageView.frame = CGRect(x: 10, y: 10, width: 20, height: 20)
        textField.addSubview(imageView)
        let leftView = UIView.init(frame: CGRect(x:10,y: 0,width: 30,height: 30))
        textField.leftView = leftView;
        textField.leftViewMode = UITextFieldViewMode.always
    }
    
}
