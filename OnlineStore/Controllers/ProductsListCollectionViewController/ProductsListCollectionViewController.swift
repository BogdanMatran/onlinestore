//
//  ProductsListCollectionViewController.swift
//  OnlineStore
//
//  Created by Matran, Bogdan on 14/06/2018.
//  Copyright © 2018 Matran, Bogdan. All rights reserved.
//

import Foundation
import UIKit

class ProductsListCollectionViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    let collectionViewMargins: CGFloat = 15
    let collectionViewCellHeight: CGFloat = 180
    let numberOfCellsPerRow: CGFloat = 2
    var products: [Product]?
    
    override func viewDidLoad() {
       navigationItem.title = "Products"
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
        registerNibs()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let productsCount = products?.count else { return 0}
        return productsCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductsListCollectionViewCell.identifier(), for: indexPath) as? ProductsListCollectionViewCell else {
            return UICollectionViewCell()
        }
        guard let currentProduct = products?[indexPath.row] else { return UICollectionViewCell() }
        cell.decorate(product: currentProduct)
        cell.layer.cornerRadius = 10
        cell.layer.masksToBounds = true
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenWidth = UIScreen.main.bounds.size.width/numberOfCellsPerRow - collectionViewMargins
        return CGSize(width: screenWidth, height: collectionViewCellHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    private func registerNibs() {
        collectionView.register(UINib(nibName: String(describing: ProductsListCollectionViewCell.self), bundle: nil), forCellWithReuseIdentifier: ProductsListCollectionViewCell.identifier())
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let selectedProduct = products?[indexPath.row] else { return }
        self.performSegue(withIdentifier: "transitionToDetails", sender: selectedProduct)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "transitionToDetails" {
            guard let detailsVC = segue.destination as? DetailsViewController else { return }
            if let product = sender as? Product {
                detailsVC.product = product
            }
        }
    }
}
