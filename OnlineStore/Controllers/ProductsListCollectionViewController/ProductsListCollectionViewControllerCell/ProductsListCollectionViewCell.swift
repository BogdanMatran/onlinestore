//
//  ProductsListCollectionViewCell.swift
//  OnlineStore
//
//  Created by Matran, Bogdan on 14/06/2018.
//  Copyright © 2018 Matran, Bogdan. All rights reserved.
//

import UIKit

class ProductsListCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var compositionLabel: UILabel!
    
    override func awakeFromNib() {
        productImage.addShadow(with: 10, and: 0.75)
        super.awakeFromNib()
    }

    class func identifier() -> String {
        return "ProductsListCollectionViewCell"
    }
    
    func decorate(product: Product) {
        productImage.image = product.image
        nameLabel.text = product.name
        compositionLabel.text = product.composition
    }
}
