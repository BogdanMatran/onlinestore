//
//  DetailsViewCOntroller.swift
//  OnlineStore
//
//  Created by Matran, Bogdan on 22/06/2018.
//  Copyright © 2018 Matran, Bogdan. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {
    
    @IBOutlet weak var detailsImageView: UIImageView!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var compositionLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var addToCartButton: UIButton!
    @IBOutlet weak var priceLabel: UILabel!
    
    var product: Product?
    
    @IBAction func didTapOnAddToCart(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    override func viewDidLoad() {
        addToCartButton.addShadow(with: 6, and: 0.75)
        detailsImageView.addShadow(with: 6, and: 0.75)
        super.viewDidLoad()
        decorate()
    }
    
    func decorate() {
        addToCartButton.layer.cornerRadius = 8
        detailsImageView.image = product?.image
        priceLabel.text = "\(product?.price ?? 0.00) $"
        categoryLabel.text = "Name: \(product?.name ?? "xxx")"
        compositionLabel.text = "Ingredients: \(product?.composition ?? " - ")"
        descriptionLabel.text = product?.description()
    }
}

