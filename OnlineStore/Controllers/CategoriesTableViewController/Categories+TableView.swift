//
//  Categories+TableView.swift
//  OnlineStore
//
//  Created by Matran, Bogdan on 16/06/2018.
//  Copyright © 2018 Matran, Bogdan. All rights reserved.
//

import UIKit

extension CategoriesTableViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return tableViewData.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableViewData[section].opened == true {
            guard let sectionDataCount = tableViewData[section].sectionData?.count else { return 1 }
            return sectionDataCount + 1
        }
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            return mainCategoryCell(tableView: categoriesTableView, at: indexPath)
        } else {
            return simpleCategoryCell(tableView: categoriesTableView, at: indexPath)
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 && tableViewData[indexPath.section].sectionData != nil{
            //expand selected cell
            tableViewData[indexPath.section].opened = !tableViewData[indexPath.section].opened
            let sections = IndexSet.init(integer: indexPath.section)
            tableView.reloadSections(sections, with: .automatic)
        } else if indexPath.row == 0 && tableViewData[indexPath.section].sectionData == nil  {
            // go to cosmectics
            let selectedProductsType = tableViewData[indexPath.section].title
            guard let selectedProducts = getProducts(for: selectedProductsType) else { return }
            self.performSegue(withIdentifier: "transitionToProductsCollection", sender: selectedProducts)
        } else {
            // go to selected product
            if let selectedProductsType = tableViewData[indexPath.section].sectionData?[indexPath.row - 1].title {
                guard let selectedProducts = getProducts(for: selectedProductsType) else { return }
                self.performSegue(withIdentifier: "transitionToProductsCollection", sender: selectedProducts)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "transitionToProductsCollection" {
            guard let productsVC = segue.destination as? ProductsListCollectionViewController else { return }
            if let productsArray = sender as? [Product] {
                productsVC.products = productsArray
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    /// This method returns a CategoriesTableViewCell instance
    ///
    /// - Parameters:
    ///   - tableView: current table view
    ///   - indexPath: current index path
    /// - Returns: the CategoriesTableViewCell instance
    private func simpleCategoryCell(tableView: UITableView, at indexPath: IndexPath) -> CategoriesTableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CategoriesTableViewCell.cellIdentifier(), for: indexPath) as! CategoriesTableViewCell
        cell.selectionStyle = .none
        cell.decorate(with: tableViewData[indexPath.section].sectionData?[indexPath.row - 1].image, and: tableViewData[indexPath.section].sectionData?[indexPath.row - 1].title)
        cell.accessoryView = UIImageView(image: #imageLiteral(resourceName: "right-arrow"))
        return cell
    }
    
   
    /// This method returns a CategoriesTableViewMainCell instance
    ///
    /// - Parameters:
    ///   - tableView: current table view
    ///   - indexPath: current index path
    /// - Returns: the CategoriesTableViewMainCell instance
    private func mainCategoryCell(tableView: UITableView, at indexPath: IndexPath) -> CategoriesTableViewMainCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CategoriesTableViewMainCell.cellIdentifier(), for: indexPath) as! CategoriesTableViewMainCell
        cell.selectionStyle = .none
        cell.decorateWith(image: tableViewData[indexPath.section].image, title: tableViewData[indexPath.section].title)
        setupAccessoryView(for: cell, at: indexPath)
        return cell
    }
    
    /// This method will setup the accessory view for CategoriesTableViewMainCell
    ///
    /// - Parameters:
    ///   - categoriesMainCell: CategoriesTableViewMainCell
    ///   - indexPath: current index path
    private func setupAccessoryView(for categoriesMainCell: CategoriesTableViewMainCell, at indexPath: IndexPath) {
        if tableViewData[indexPath.section].sectionData != nil {
            if tableViewData[indexPath.section].opened == true {
                categoriesMainCell.accessoryView = UIImageView(image: #imageLiteral(resourceName: "up-arrow"))
            } else {
                categoriesMainCell.accessoryView = UIImageView(image: #imageLiteral(resourceName: "down-arrow"))
            }
        } else {
            categoriesMainCell.accessoryView = UIImageView(image: #imageLiteral(resourceName: "right-arrow"))
        }
    }
}
