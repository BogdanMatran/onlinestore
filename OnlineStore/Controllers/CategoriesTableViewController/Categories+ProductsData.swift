//
//  Categories+ProductsData.swift
//  OnlineStore
//
//  Created by Matran, Bogdan on 16/06/2018.
//  Copyright © 2018 Matran, Bogdan. All rights reserved.
//

import Foundation

extension CategoriesTableViewController {
    func getProducts(for type: String) -> [Product]? {
        switch type {
        case ProductsType.Backery.rawValue:
            let bread = Bakery(name: "Bread", composition: "Flour, salt, water", image: #imageLiteral(resourceName: "bread"), price: 12.3, expireDate: "23.06.2012", glutenFree: true, flourType: .breadFlour)
            let baguette = Bakery(name: "Baguette", composition: "Flour, salt, water", image: #imageLiteral(resourceName: "baguette"), price: 10.0, expireDate: "23.06.2012", glutenFree: true, flourType: .breadFlour)
            let buns = Bakery(name: "Buns", composition: "Flour, milk, sugar, eggs", image: #imageLiteral(resourceName: "buns"), price: 9.00, expireDate: "10.07.2018", glutenFree: false, flourType: .cakeFlour)
            let croissant = Bakery(name: "Croissant", composition: "Flour, milk, sugar, eggs", image: #imageLiteral(resourceName: "croissant"), price: 9.50, expireDate: "10.07.2018", glutenFree: false, flourType: .cakeFlour)
            let sandwich = Bakery(name: "Sandwich", composition: "Flour, salt, water, salami", image: #imageLiteral(resourceName: "sandwich"), price: 11.00, expireDate: "10.07.2018", glutenFree: false, flourType: .breadFlour)
            let doughnut = Bakery(name: "Doughnut", composition: "Flour, milk, sugar, eggs ", image: #imageLiteral(resourceName: "doughnut"), price: 2.00, expireDate: "10.08.2018", glutenFree: false, flourType: .cakeFlour)
            let cake = Bakery(name: "Cake", composition: "Flour, milk, sugar, eggs", image: #imageLiteral(resourceName: "cake"), price: 100.20, expireDate: "20.09.2018", glutenFree: false, flourType: .cakeFlour)
            return [bread, baguette, buns, croissant, sandwich, doughnut, cake]
            
        case ProductsType.Clothes.rawValue:
           let tshirt = Clothes(name: "Tshirt", compozition: "Silk", image: #imageLiteral(resourceName: "shirt"), price: 50.00, size: "M", gender: .M, season: .summer, typeOfClothes: "Running")
           let poloShirt = Clothes(name: "Polo shirt", compozition: "Silk 100%", image: #imageLiteral(resourceName: "poloshirt"), price: 10.29, size: "XL", gender: .M, season: .summer, typeOfClothes: "Casual")
           let basketballJersey = Clothes(name: "Basketball Jersey", compozition: "Coton", image: #imageLiteral(resourceName: "basketball-jersey"), price: 120.0, size: "M, XL, XXL", gender: .M, season: .autumn, typeOfClothes: "Sports")
           let shorts = Clothes(name: "Shortes", compozition: "Coton", image: #imageLiteral(resourceName: "shorts"), price: 20.00, size: "S, M, L", gender: .U, season: .summer, typeOfClothes: "Home")
           let trousers = Clothes(name: "Trousers", compozition: "Coton", image: #imageLiteral(resourceName: "trousers"), price: 40.00, size: "XS, S, M", gender: .F, season: .summer, typeOfClothes: "Gym")
           let jeans = Clothes(name: "Jeans", compozition: "Jeans", image: #imageLiteral(resourceName: "jeans"), price: 210.00, size: "30 - 42", gender: .U, season: .autumn, typeOfClothes: "Casual")
           let longDress = Clothes(name: "Long Dress", compozition: "Silk", image: #imageLiteral(resourceName: "longdress"), price: 150.00, size: "S, M", gender: .F, season: .summer, typeOfClothes: "Casual")
           let santaClausHat = Clothes(name: "Santa Claus Hat", compozition: "Coton", image: #imageLiteral(resourceName: "santa-claus"), price: 40.00, size: "Regular", gender: .U, season: .winter, typeOfClothes: "Funny")
           let winterHat = Clothes(name: "Winter Hat", compozition: "Coton", image: #imageLiteral(resourceName: "hat"), price: 60.00, size: "Regular", gender: .M, season: .winter, typeOfClothes: "Warm")
           return [tshirt, poloShirt, basketballJersey, shorts, trousers, jeans, longDress, santaClausHat, winterHat]
            
        case ProductsType.Dairy.rawValue:
            let milk = Dairy(name: "Milk", composition: "milk", image: #imageLiteral(resourceName: "milk"), price: 6.00, expireDate: "10.07.2018", milkType: .cowMilk, fatsPercent: 5.00)
            let butter = Dairy(name: "Butter", composition: "milk", image: #imageLiteral(resourceName: "butter"), price: 8.70, expireDate: "10.08.2019", milkType: .sheepMilk, fatsPercent: 10.00)
            let cheese = Dairy(name: "Cheese", composition: "milk", image: #imageLiteral(resourceName: "cheese"), price: 11.20, expireDate: "23.08.2018", milkType: .goatMilk, fatsPercent: 30.00)
            let smellyCheese = Dairy(name: "Smelly Cheese", composition: "milk", image: #imageLiteral(resourceName: "smellyCheese"), price: 20.20, expireDate: "20.12.1028", milkType: .buffalosMilk, fatsPercent: 20.00)
            return [milk, butter, cheese, smellyCheese]
            
        case ProductsType.Footwear.rawValue:
            let shoes = Footwear(name: "Shoes", compozition: "Leather, Plastic", image: #imageLiteral(resourceName: "shoe"), price: 200, size: "40-44", gender: .M, typeOfFootwear: .shoes)
            let slippers = Footwear(name: "Slippers", compozition: "Foam", image: #imageLiteral(resourceName: "slippers"), price: 60.00, size: "35-39", gender: .F, typeOfFootwear: .sandals)
            let boots = Footwear(name: "Boots", compozition: "Leather", image: #imageLiteral(resourceName: "boots"), price: 250.00, size: "35-39", gender: .F, typeOfFootwear: .boots)
            let sneakers = Footwear(name: "Sneakers", compozition: "Textil", image: #imageLiteral(resourceName: "sneakers"), price: 200.00, size: "40-46", gender: .M, typeOfFootwear: .sneackers)
            let highHeel = Footwear(name: "High Heel", compozition: "Leather", image: #imageLiteral(resourceName: "high-heel-red"), price: 150.00, size: "35-39", gender: .F, typeOfFootwear: .shoes)
            let trainers = Footwear(name: "Trainers", compozition: "Textil", image: #imageLiteral(resourceName: "trainer"), price: 170.00, size: "40-46", gender: .M, typeOfFootwear: .shoes)
            return [shoes, slippers, boots, sneakers, highHeel, trainers]
            
        case ProductsType.Cosmetics.rawValue:
            let sunCream = Cosmetics(name: "Sun Cream", composition: "Anti UV , Fats ", image: #imageLiteral(resourceName: "sunCream"), price: 10.00, typeOfUsage: .primer)
            let cream = Cosmetics(name: "Cream", composition: "Fats, Colors", image: #imageLiteral(resourceName: "cream"), price: 8.00, typeOfUsage: .foundation)
            let makeupKit = Cosmetics(name: "Makeup Kit", composition: "Colors , Brushes", image: #imageLiteral(resourceName: "makeupKit"), price: 50.00, typeOfUsage: .foundation)
            let lipstick = Cosmetics(name: "Lipstick", composition: "Cream, Fats", image: #imageLiteral(resourceName: "lipstick"), price: 16.00, typeOfUsage: .lipstick)
            let brush = Cosmetics(name: "Brush", composition: "Brush", image: #imageLiteral(resourceName: "brush"), price: 21.00, typeOfUsage: .primer)
            let nailPolish = Cosmetics(name: "Nail Polish", composition: "Oils", image: #imageLiteral(resourceName: "nail-polish"), price: 30.00, typeOfUsage: .nailPolish)
            return [sunCream, cream, makeupKit, lipstick, brush, nailPolish]
            
        default:
            return nil
        }
    }
}
