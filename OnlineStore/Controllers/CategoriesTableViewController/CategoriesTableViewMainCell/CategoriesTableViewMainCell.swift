//
//  CategoriesTableViewMainCell.swift
//  OnlineStore
//
//  Created by Matran, Bogdan on 13/06/2018.
//  Copyright © 2018 Matran, Bogdan. All rights reserved.
//

import UIKit

class CategoriesTableViewMainCell: UITableViewCell {

    @IBOutlet weak var categoryImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        categoryImageView.addShadow(with: 6, and: 0.75)
    }
    
    class func  cellIdentifier() -> String {
        return "CategoriesTableViewMainCell"
    }
    
    func decorateWith(image: UIImage?, title: String?) {
        categoryImageView.image = image
        titleLabel.text = title
    }
}
