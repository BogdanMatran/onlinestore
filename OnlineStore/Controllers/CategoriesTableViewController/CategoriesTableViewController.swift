//
//  CategoriesTableViewController.swift
//  OnlineStore
//
//  Created by Matran, Bogdan on 13/06/2018.
//  Copyright © 2018 Matran, Bogdan. All rights reserved.
//

import Foundation
import UIKit

struct CellData {
    var opened = false
    var title: String = ""
    var image: UIImage = UIImage()
    var sectionData: [SectionData]?
}

struct SectionData {
    var title: String = ""
    var image: UIImage = UIImage()
}

enum ProductsType: String {
    case Cosmetics = "Cosmetics"
    case Backery = "Bakery"
    case Dairy = "Dairy"
    case Footwear = "Footwear"
    case Clothes = "Clothes"
}

class CategoriesTableViewController: UITableViewController {
    
    @IBOutlet var categoriesTableView: UITableView!
    var tableViewData = [CellData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        checkConnectionData()
        setupNavigationBar()
        setupTableView()
        createTableViewData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = false
    }
    
    private func setupTableView() {
        categoriesTableView.register(UINib(nibName: "CategoriesTableViewCell", bundle: nil), forCellReuseIdentifier: CategoriesTableViewCell.cellIdentifier())
        categoriesTableView.register(UINib(nibName: "CategoriesTableViewMainCell", bundle: nil), forCellReuseIdentifier: CategoriesTableViewMainCell.cellIdentifier())
        categoriesTableView.separatorStyle = .none
    }
    
    private func checkConnectionData() {
        if UserDefaults.standard.containsConnectData() == false {
            self.performSegue(withIdentifier: "editConnectTransition", sender: nil)
        }
    }
    
    private func setupNavigationBar() {
        let profileButton = UIBarButtonItem(title: nil, style: .plain, target: self, action: #selector(didTapOnEditInformations))
        profileButton.image = #imageLiteral(resourceName: "user-connect")
        profileButton.tintColor = UIColor.lightGray
        navigationItem.rightBarButtonItem = profileButton
        navigationItem.title = "Market"
    }
    
    private func createTableViewData() {
        tableViewData = [CellData(opened: true, title: ProductsType.Cosmetics.rawValue, image: #imageLiteral(resourceName: "make-up-menu"), sectionData: nil),
                         CellData(opened: true, title: "Food", image: #imageLiteral(resourceName: "groceries"), sectionData: [SectionData(title: ProductsType.Backery.rawValue, image: #imageLiteral(resourceName: "bread-menu")), SectionData(title: ProductsType.Dairy.rawValue, image: #imageLiteral(resourceName: "cheese-menu"))]),
                         CellData(opened: true, title: "Clothing", image: #imageLiteral(resourceName: "clothes"), sectionData: [SectionData(title: ProductsType.Footwear.rawValue, image: #imageLiteral(resourceName: "shoe")), SectionData(title: ProductsType.Clothes.rawValue, image: #imageLiteral(resourceName: "fashionmenu"))])]
    }
    
    @objc func didTapOnEditInformations() {
        print("Tapped")
        self.performSegue(withIdentifier: "editConnectTransition", sender: nil)
    }
}
