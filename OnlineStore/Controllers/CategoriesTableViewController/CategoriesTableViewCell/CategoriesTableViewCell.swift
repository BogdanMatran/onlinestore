//
//  CategoriesTableViewCell.swift
//  OnlineStore
//
//  Created by Matran, Bogdan on 13/06/2018.
//  Copyright © 2018 Matran, Bogdan. All rights reserved.
//

import UIKit

class CategoriesTableViewCell: UITableViewCell {

    @IBOutlet weak var imageProduct: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        imageProduct.addShadow(with: 10, and: 0.75)
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    class func  cellIdentifier() -> String {
        return "CategoriesTableViewCell"
    }
    
    func decorate(with image: UIImage?, and title: String?) {
        titleLabel.text = title
        imageProduct.image = image
    }
}
