//
//  Products.swift
//  OnlineStore
//
//  Created by Matran, Bogdan on 13/06/2018.
//  Copyright © 2018 Matran, Bogdan. All rights reserved.
//

import Foundation
import UIKit

protocol Product {
    var name: String { get set }
    var composition: String { get set }
    var image: UIImage { get set }
    var price: Float { get set }
    
    func description() -> String
}
