//
//  Clothing.swift
//  OnlineStore
//
//  Created by Matran, Bogdan on 13/06/2018.
//  Copyright © 2018 Matran, Bogdan. All rights reserved.
//

import Foundation
import UIKit

enum Gender {
    case M
    case F
    case U
}

class Clothing: Product {
    func description() -> String {
        return ""
    }
    
    
    var name: String
    var composition: String
    var image: UIImage
    var price: Float
    var size: String
    var gender: Gender
    
    init(name: String, compozition: String, image: UIImage, price: Float, size: String, gender: Gender) {
        self.name = name
        self.composition = compozition
        self.image = image
        self.price = price
        self.size = size
        self.gender = gender
    }
}
