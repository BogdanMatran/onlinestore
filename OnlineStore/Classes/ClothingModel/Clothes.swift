//
//  Clothes.swift
//  OnlineStore
//
//  Created by Matran, Bogdan on 13/06/2018.
//  Copyright © 2018 Matran, Bogdan. All rights reserved.
//

import Foundation
import UIKit

enum Season: String {
    
    case summer
    case autumn
    case winter
    case spring
}

class Clothes: Clothing {
    
    var season: Season
    var typeOfClothes: String
    
    init(name: String, compozition: String, image: UIImage, price: Float, size: String, gender: Gender, season: Season, typeOfClothes: String) {
        self.season = season
        self.typeOfClothes = typeOfClothes
        super.init(name: name, compozition: compozition, image: image, price: price, size: size, gender: gender)
    }
    
    override func description() -> String {
        return "\(name) with composition \(composition), price \(price) available sizes \(size) for gender \(gender), season \(season.rawValue) with type \(typeOfClothes)"
    }
}
