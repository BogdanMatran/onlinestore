//
//  Footwear.swift
//  OnlineStore
//
//  Created by Matran, Bogdan on 13/06/2018.
//  Copyright © 2018 Matran, Bogdan. All rights reserved.
//

import Foundation
import UIKit

enum TypeOfFootwear: String {
    case shoes
    case boots
    case sandals
    case indoorFootwear
    case traditionalFootwear
    case sneackers
    case socks
}

class Footwear: Clothing {
    var typeOfFootwear: TypeOfFootwear
    
    init(name: String, compozition: String, image: UIImage, price: Float, size: String, gender: Gender, typeOfFootwear: TypeOfFootwear) {
        self.typeOfFootwear = typeOfFootwear
        super.init(name: name, compozition: compozition, image: image, price: price, size: size, gender: gender)
    }
    
    override func description() -> String {
        return "\(name) with composition \(composition), price \(price) available sizes \(size) for gender \(gender) and type \(typeOfFootwear.rawValue)"
    }
}
