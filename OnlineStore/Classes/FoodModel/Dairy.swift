//
//   Dairy.swift
//  OnlineStore
//
//  Created by Matran, Bogdan on 13/06/2018.
//  Copyright © 2018 Matran, Bogdan. All rights reserved.
//

import Foundation
import UIKit

enum MilkType: String {
    case cowMilk
    case buffalosMilk
    case goatMilk
    case sheepMilk
}

class Dairy: Food {
    var milkType: MilkType
    var fatsPercent: Float
    init(name: String, composition: String, image: UIImage, price: Float, expireDate: String, milkType: MilkType, fatsPercent: Float) {
        self.milkType = milkType
        self.fatsPercent = fatsPercent
        super.init(name: name, composition: composition, image: image, price: price, expireDate: expireDate)
    }
    
    override func description() -> String {
        return "\(name) with composition \(composition), price \(price) and expire date \(expireDate) from \(milkType.rawValue) "
    }
}
