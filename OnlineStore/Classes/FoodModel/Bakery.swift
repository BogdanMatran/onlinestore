//
//  Bakery.swift
//  OnlineStore
//
//  Created by Matran, Bogdan on 13/06/2018.
//  Copyright © 2018 Matran, Bogdan. All rights reserved.
//

import Foundation
import UIKit

enum FlourType: String {
    case allPurposeFlour
    case cakeFlour
    case pastryFlour
    case breadFlour
    case glutenFreeFlour
}

class Bakery: Food {
    var glutenFree: Bool
    var flourType: FlourType
    init(name: String, composition: String, image: UIImage, price: Float, expireDate: String, glutenFree: Bool, flourType: FlourType) {
        self.glutenFree = glutenFree
        self.flourType = flourType
        super.init(name: name, composition: composition, image: image, price: price, expireDate: expireDate)
    }
    
    override func description() -> String {
        if glutenFree {
        return "\(name) with composition \(composition), price \(price) and expire date \(expireDate) from \(flourType.rawValue) is gluten free"
        } else {
            return "\(name) with composition \(composition), price \(price) and expire date \(expireDate) from \(flourType.rawValue) is gluten free"
        }
    }
}
