//
//  Food.swift
//  OnlineStore
//
//  Created by Matran, Bogdan on 13/06/2018.
//  Copyright © 2018 Matran, Bogdan. All rights reserved.
//

import Foundation
import UIKit

class Food: Product {
    func description() -> String {
        return ""
    }
    
    
    var name: String
    var composition: String
    var image: UIImage
    var price: Float
    var expireDate: String
    
    init(name: String, composition: String, image: UIImage, price: Float, expireDate: String) {
        self.name = name
        self.composition = composition
        self.image = image
        self.price = price
        self.expireDate = expireDate
    }
}
