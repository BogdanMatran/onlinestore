//
//  Cosmetics.swift
//  OnlineStore
//
//  Created by Matran, Bogdan on 13/06/2018.
//  Copyright © 2018 Matran, Bogdan. All rights reserved.
//

import Foundation
import UIKit

enum TypeOfUsage: String {
    case primer
    case lipstick
    case concealer
    case foundation
    case facePowder
    case eyeLiner
    case nailPolish
}

class Cosmetics: Product {
    func description() -> String {
        return "\(name) with composition \(composition) ,price \(price) and type of usage \(typeOfUsage.rawValue)"
    }
    
    var name: String
    var composition: String
    var image: UIImage
    var price: Float
    var typeOfUsage: TypeOfUsage
    
    init(name: String, composition: String, image: UIImage, price: Float, typeOfUsage: TypeOfUsage) {
        self.name = name
        self.composition = composition
        self.image = image
        self.price = price
        self.typeOfUsage = typeOfUsage
    }
}
