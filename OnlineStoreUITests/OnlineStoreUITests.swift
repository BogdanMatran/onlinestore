//
//  OnlineStoreUITests.swift
//  OnlineStoreUITests
//
//  Created by Matran, Bogdan on 26/06/2018.
//  Copyright © 2018 Matran, Bogdan. All rights reserved.
//

import XCTest

class OnlineStoreUITests: XCTestCase {
    var app: XCUIApplication!
    fileprivate let nameTextFieldIdentifier = "Full Name"
    fileprivate let connectButtonIdentifier = "Connect"
    fileprivate let phoneTextFieldIdentifier = "Phone"
    fileprivate let connectAndSaveButtonIdentifier = "Connect & Save"
    fileprivate let addressTextViewIdentifier = "Address"
    fileprivate let disconnectButtonIdentifier = "Disconnect"
    
    override func setUp() {
        super.setUp()
        app = XCUIApplication()
        app.launch()
    }
    
    func typeName() {
        let bKey = app/*@START_MENU_TOKEN@*/.keys["b"]/*[[".keyboards.keys[\"b\"]",".keys[\"b\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        bKey.tap()
        let oKey = app/*@START_MENU_TOKEN@*/.keys["o"]/*[[".keyboards.keys[\"o\"]",".keys[\"o\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        oKey.tap()
        let gKey = app/*@START_MENU_TOKEN@*/.keys["g"]/*[[".keyboards.keys[\"g\"]",".keys[\"g\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        gKey.tap()
        let dKey = app/*@START_MENU_TOKEN@*/.keys["d"]/*[[".keyboards.keys[\"d\"]",".keys[\"d\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        dKey.tap()
        let aKey = app/*@START_MENU_TOKEN@*/.keys["a"]/*[[".keyboards.keys[\"a\"]",".keys[\"a\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        aKey.tap()
        let nKey = app/*@START_MENU_TOKEN@*/.keys["n"]/*[[".keyboards.keys[\"n\"]",".keys[\"n\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        nKey.tap()
    }
    
    func typePhone() {
        let moreKey = app/*@START_MENU_TOKEN@*/.keys["more"]/*[[".keyboards",".keys[\"more, numbers\"]",".keys[\"more\"]"],[[[-1,2],[-1,1],[-1,0,1]],[[-1,2],[-1,1]]],[0]]@END_MENU_TOKEN@*/
        moreKey.tap()
        app/*@START_MENU_TOKEN@*/.keys["1"]/*[[".keyboards.keys[\"1\"]",".keys[\"1\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        let key = app/*@START_MENU_TOKEN@*/.keys["2"]/*[[".keyboards.keys[\"2\"]",".keys[\"2\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        key.tap()
        app/*@START_MENU_TOKEN@*/.keys["3"]/*[[".keyboards.keys[\"3\"]",".keys[\"3\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        let key2 = app/*@START_MENU_TOKEN@*/.keys["4"]/*[[".keyboards.keys[\"4\"]",".keys[\"4\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        key2.tap()
    }
    
    func typeAddress() {
        let tKey = app/*@START_MENU_TOKEN@*/.keys["T"]/*[[".keyboards.keys[\"T\"]",".keys[\"T\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        tKey.tap()
        let eKey = app/*@START_MENU_TOKEN@*/.keys["e"]/*[[".keyboards.keys[\"e\"]",".keys[\"e\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        eKey.tap()
        let sKey = app/*@START_MENU_TOKEN@*/.keys["s"]/*[[".keyboards.keys[\"s\"]",".keys[\"s\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        sKey.tap()
        let tKey2 = app/*@START_MENU_TOKEN@*/.keys["t"]/*[[".keyboards.keys[\"t\"]",".keys[\"t\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        tKey2.tap()
    }
    
    //given
    func testNameLabelIsEnabled() {
        XCTAssertTrue(app.textFields[nameTextFieldIdentifier].exists)
    }
    
    func testNameTextFieldPlaceholder() {
        XCTAssertTrue(app.textFields[nameTextFieldIdentifier].placeholderValue == nameTextFieldIdentifier)
    }
    
    func testPhoneTextFieldPlaceholder() {
        XCTAssertTrue(app.textFields[phoneTextFieldIdentifier].placeholderValue == phoneTextFieldIdentifier)
    }
    
    func testPhoneTextViewIsEnabled() {
        XCTAssertTrue(app.textFields[phoneTextFieldIdentifier].exists)
    }
    
    func testAddressTextViewIsEnabled() {
        let addressTextView = app.scrollViews.otherElements.otherElements[addressTextViewIdentifier]
        XCTAssertTrue(addressTextView.exists)
    }
    
    func testConnectButtonIsEnabled() {
        XCTAssertTrue(app.buttons[connectButtonIdentifier].isEnabled)
    }
   
    func testConnectAndSaveButtonIsEnabled() {
        XCTAssertTrue(app.buttons[connectAndSaveButtonIdentifier].isEnabled)
    }
   
    func testDissconnectButtonIsEnabled() {
        XCTAssertTrue(app.buttons[disconnectButtonIdentifier].isEnabled)
    }
    //then
    func testLogin() {
        let elementsQuery = app.scrollViews.otherElements
        elementsQuery.textFields[nameTextFieldIdentifier].tap()
        typeName()
        elementsQuery.textFields[phoneTextFieldIdentifier].tap()
        typePhone()
        elementsQuery.otherElements[addressTextViewIdentifier].tap()
        typeAddress()
        app/*@START_MENU_TOKEN@*/.buttons["Done"]/*[[".keyboards.buttons[\"Done\"]",".buttons[\"Done\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        elementsQuery.buttons["Connect"].tap()
        
    }
    
    func testValidConnect() {
        let nameTextField = app.textFields[nameTextFieldIdentifier]
        let addressTextView = app.scrollViews.otherElements.otherElements[addressTextViewIdentifier]
        let connectButton = app.buttons[connectButtonIdentifier]
        let disconnectButton = app.buttons[disconnectButtonIdentifier]
        disconnectButton.tap()
        nameTextField.tap()
        typeName()
        app.keyboards.buttons["Next"].tap()
        typePhone()
        app.keyboards.buttons["Done"].tap()
        addressTextView.tap()
        typeAddress()
        app.keyboards.buttons["Done"].tap()
        connectButton.tap()
        let menuTable = app.tables
        XCTAssertEqual(menuTable.cells.count, 7, "Main menu with table view")
    }
    
    func testInvalidConnectMissingAddress() {
        let nameTextField = app.textFields[nameTextFieldIdentifier]
        let phoneTextField = app.textFields[phoneTextFieldIdentifier]
        let connectButton = app.buttons[connectButtonIdentifier]
        let disconnectButton = app.buttons[disconnectButtonIdentifier]
        disconnectButton.tap()
        nameTextField.tap()
        nameTextField.typeText("Bogdan")
        app.keyboards.buttons["Next"].tap()
        phoneTextField.typeText("0743795003")
        app.keyboards.buttons["Done"].tap()
        connectButton.tap()
        let alert = app.alerts.firstMatch
        XCTAssertEqual(alert.label, "Error")
    }
    
    func testInvalidConnectMissingName() {
        let phoneTextField = app.textFields[phoneTextFieldIdentifier]
        let addressTextView = app.scrollViews.otherElements.otherElements[addressTextViewIdentifier]
        let connectButton = app.buttons[connectAndSaveButtonIdentifier]
        let disconnectButton = app.buttons[disconnectButtonIdentifier]
        disconnectButton.tap()
        phoneTextField.tap()
        typePhone()
        if app.keyboards.buttons["Done"].isEnabled {
            app.keyboards.buttons["Done"].tap()
        }
        addressTextView.tap()
        typeAddress()
        app.keyboards.buttons["Done"].tap()
        connectButton.tap()
        let alert = app.alerts.firstMatch
        XCTAssertEqual(alert.label, "Error")
    }
    
    func testInvalidConnectMissingPhone() {
        let nameTextField = app.textFields[nameTextFieldIdentifier]
        let addressTextView = app.scrollViews.otherElements.otherElements[addressTextViewIdentifier]
        let connectButton = app.buttons[connectAndSaveButtonIdentifier]
        let disconnectButton = app.buttons[disconnectButtonIdentifier]
        disconnectButton.tap()
        nameTextField.tap()
        typeName()
        addressTextView.tap()
        typeAddress()
        app.keyboards.buttons["Done"].tap()
        connectButton.tap()
        let alert = app.alerts.firstMatch
        XCTAssertEqual(alert.label, "Error")
    }
    
    func testMenuItems() {
        testLogin()
        let tablesQuery = app.tables
        XCTAssertEqual(tablesQuery.cells.count, 7, "Number of cells in table")
        
    }
    
    func testExpandFoodSection() {
        testLogin()
        let backery = app.staticTexts["Bakery"]
        let dairy = app.staticTexts["Dairy"]
        XCTAssertTrue(backery.exists)
        XCTAssertTrue(dairy.exists)
        let food = app.staticTexts["Food"]
        XCTAssertTrue(food.exists)
        food.tap()
        XCTAssertFalse(backery.exists)
        XCTAssertFalse(dairy.exists)
    }
    
    func testExpandClothingSection() {
        testLogin()
        let clothing = app.staticTexts["Clothing"]
        XCTAssertTrue(clothing.exists)
        let footwear = app.staticTexts["Footwear"]
        let clothes = app.staticTexts["Clothes"]
        XCTAssertTrue(footwear.exists)
        XCTAssertTrue(clothes.exists)
        clothing.tap()
        XCTAssertFalse(footwear.exists)
        XCTAssertFalse(clothes.exists)
    }
    
    override func tearDown() {
        super.tearDown()
        let screenshot = XCUIScreen.main.screenshot()
        let fullScreenshotAttachment = XCTAttachment(screenshot: screenshot)
        fullScreenshotAttachment.lifetime = .keepAlways
        add(fullScreenshotAttachment)
        app.terminate()
    }
    
    func testOpenAndCountCosmetics() {
        testLogin()
        let cosmetics = app.staticTexts["Cosmetics"]
        XCTAssertTrue(cosmetics.exists)
        cosmetics.tap()
        let collectionView = app.collectionViews
        XCTAssertEqual(collectionView.cells.count, 6, "Number of cosmetics products")
    }
    
    func testOpenAndCountBakeryProducts() {
        testLogin()
        let bakery = app.staticTexts["Bakery"]
        XCTAssertTrue(bakery.exists)
        bakery.tap()
        let collectionView = app.collectionViews
        XCTAssertEqual(collectionView.cells.count, 7, "Number of bakery products")
    }
    
    func testOpenAndCountDairyProducts() {
        testLogin()
        let dairy = app.staticTexts["Dairy"]
        XCTAssertTrue(dairy.exists)
        dairy.tap()
        let collectionView = app.collectionViews
        XCTAssertEqual(collectionView.cells.count, 4, "Number of dairy products")
    }
    
    func testOpenAndCountFootwearProducts() {
        testLogin()
        let footwear = app.staticTexts["Footwear"]
        XCTAssertTrue(footwear.exists)
        footwear.tap()
        let collectionView = app.collectionViews
        XCTAssertEqual(collectionView.cells.count, 6, "Number of footwear products")
    }
    
    func testOpenAndCountClothesProducts() {
        testLogin()
        let clothes = app.staticTexts["Clothes"]
        XCTAssertTrue(clothes.exists)
        clothes.tap()
        let collectionView = app.collectionViews
        app.swipeUp()
        XCTAssertEqual(collectionView.cells.count, 9, "Number of clothes")
    }
    
    func testCosmeticsProductsNames() {
        testOpenAndCountCosmetics()
        let collectionView = app.collectionViews
        XCTAssertTrue(collectionView.cells.element(boundBy: 0).staticTexts["Sun Cream"].exists)
        XCTAssertTrue(collectionView.cells.element(boundBy: 1).staticTexts["Cream"].exists)
        XCTAssertTrue(collectionView.cells.element(boundBy: 2).staticTexts["Makeup Kit"].exists)
        XCTAssertTrue(collectionView.cells.element(boundBy: 3).staticTexts["Lipstick"].exists)
        XCTAssertTrue(collectionView.cells.element(boundBy: 4).staticTexts["Brush"].exists)
        XCTAssertTrue(collectionView.cells.element(boundBy: 5).staticTexts["Nail Polish"].exists)
    }
    
    func testCosmeticsProductsComposition() {
        testOpenAndCountCosmetics()
        let collectionView = app.collectionViews
        XCTAssertTrue(collectionView.cells.textFields["Anti UV , Fats"].exists)
        XCTAssertTrue(collectionView.cells.element(boundBy: 1).staticTexts["Fats, Colors"].exists)
        XCTAssertTrue(collectionView.cells.element(boundBy: 2).staticTexts["Colors, Brushes"].exists)
        XCTAssertTrue(collectionView.cells.element(boundBy: 3).staticTexts["Cream, Fats"].exists)
        XCTAssertTrue(collectionView.cells.element(boundBy: 4).staticTexts["Brush"].exists)
        XCTAssertTrue(collectionView.cells.element(boundBy: 5).staticTexts["Oils"].exists)
    }
    
    func testBakeryProductsNames() {
        testOpenAndCountBakeryProducts()
        let collectionView = app.collectionViews
        XCTAssertTrue(collectionView.cells.element(boundBy: 0).staticTexts["Bread"].exists)
        XCTAssertTrue(collectionView.cells.element(boundBy: 1).staticTexts["Baguette"].exists)
        XCTAssertTrue(collectionView.cells.element(boundBy: 2).staticTexts["Buns"].exists)
        XCTAssertTrue(collectionView.cells.element(boundBy: 3).staticTexts["Croissant"].exists)
        XCTAssertTrue(collectionView.cells.element(boundBy: 4).staticTexts["Sandwich"].exists)
        XCTAssertTrue(collectionView.cells.element(boundBy: 5).staticTexts["Doughnut"].exists)
        XCTAssertTrue(collectionView.cells.element(boundBy: 6).staticTexts["Cake"].exists)
    }
    
    func testBakeryProductsComposition() {
        testOpenAndCountBakeryProducts()
        let collectionView = app.collectionViews
        XCTAssertTrue(collectionView.cells.element(boundBy: 0).staticTexts["Flour, salt, water"].exists)
        XCTAssertTrue(collectionView.cells.element(boundBy: 1).staticTexts["Flour, salt, water"].exists)
        XCTAssertTrue(collectionView.cells.element(boundBy: 2).staticTexts["Flour, milk, sugar, eggs"].exists)
        XCTAssertTrue(collectionView.cells.element(boundBy: 3).staticTexts["Flour, milk, sugar, eggs"].exists)
        XCTAssertTrue(collectionView.cells.element(boundBy: 4).staticTexts["Flour, salt, water, salami"].exists)
        XCTAssertTrue(collectionView.cells.element(boundBy: 5).staticTexts.element(boundBy: 0).staticTexts["Flour, salt, water"].exists)
        XCTAssertTrue(collectionView.cells.element(boundBy: 6).staticTexts["Flour, milk, sugar, eggs"].exists)
    }
//    func testValidConnectAndSave() {
//        let elementsQuery = app.scrollViews.otherElements
//        let logoutButton = app.navigationBars.buttons.element(boundBy: 0)
//        logoutButton.tap()
//        elementsQuery.buttons["Disconnect"].tap()
//        elementsQuery.textFields[nameTextFieldIdentifier].tap()
//        typeName()
//        elementsQuery.textFields[phoneTextFieldIdentifier].tap()
//        typePhone()
//        elementsQuery.otherElements[addressTextViewIdentifier].tap()
//        typeAddress()
//        app/*@START_MENU_TOKEN@*/.buttons["Done"]/*[[".keyboards.buttons[\"Done\"]",".buttons[\"Done\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
//        elementsQuery.buttons[connectAndSaveButtonIdentifier].tap()
//        let defaults  = UserDefaults.standard
//        sleep(3)
//        print("XXXXXXXXX")
//        print(defaults.value(forKey: "name"))
//       // XCTAssertEqual(defaults.value(forKey: "name") as! String, "isa", "User defaults login name")
//
//    }
}
